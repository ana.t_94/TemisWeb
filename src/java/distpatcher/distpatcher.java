package distpatcher;

import controller.ApartmentController;
import controller.ChargeController;
import controller.DebtController;
import controller.UserController;
import facade.ApartmentFacade;
import facade.ChargeFacade;
import facade.DebtFacade;
import facade.UserFacade;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/distpatcher"})
public class distpatcher extends HttpServlet {

    @EJB
    private ChargeFacade chargeFacade;

    @EJB
    private DebtFacade debtFacade;

    @EJB
    private ApartmentFacade apartmentFacade;

    @EJB
    private UserFacade userFacade;
    
    private ChargeController chargeController;
    private DebtController debtController;
    private UserController userController;
    private ApartmentController apartmentController;
    
    @Override
    public void init() throws ServletException {
        super.init();
        this.debtController = new DebtController(debtFacade, chargeFacade, apartmentFacade, userFacade);
        this.apartmentController = new ApartmentController(apartmentFacade, userFacade, chargeFacade, debtFacade);
        this.chargeController = new ChargeController(chargeFacade, debtFacade, userFacade);
        this.userController = new UserController(userFacade);
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        response.setContentType("text/html;charset = UTF-8");
        
        this.apartmentController.setDistPatch(request, response);
        this.debtController.setDistPatch(request, response);
        this.chargeController.setDistPatch(request, response);
        this.userController.setDistPatch(request, response);
           
        switch (action) {
            case "login":
                this.userController.loginControl();
                break;
            case "register":
                this.userController.registerControl();
                break;
            case "modifyUser":
                this.userController.modifyUserControl();
                break;
            case "createApartment":
                this.apartmentController.createApartmentControl();
                break;
            case "selectApartment":
                this.apartmentController.selectApartmentControl();
                break;
            case "apartmentDelete":
                this.apartmentController.apartmentDeleteControl();
                break;
            case "selectDebt":
                this.debtController.selectDebtControl();
                break;
            case "goNewDebt":
                this.debtController.goDebtControl();
                break;
            case "debtBack":
                this.userController.manageUsersBack();
                break;
            case "createDebt":
                this.debtController.createDebtControl();
                break;
            case "debtDelete":
                this.debtController.debtDeleteControl();
                break;
            case "goManageUsers":
                this.userController.goManageUsersControl();
                break;
            case "manageUsersDelete":
                this.userController.manageUsersDeleteControl();
                break;
            case "manageUsersAdd":
                this.userController.manageUsersAddControl();
                break;
            case "manageUsersBack":
                this.userController.manageUsersBack();
                break;
            case "selectCharge":
                this.chargeController.selectChargeControl();
                break;
            case "chargeBack":
                this.chargeController.chargeBack();
                break;
            case "chargeModify":
                this.chargeController.chargeModifyControl();
                break;
            case "chargeDelete":
                this.chargeController.chargeDeleteControl();
                break;
            case "goNewCharge":
                this.chargeController.goChargeControl();
                break;
            case "createCharge":
                this.chargeController.createChargeControl();
                break;
            case "myApartmentsBC":
                this.apartmentController.myApartmentsBC();
                break;
        }

    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>

}
