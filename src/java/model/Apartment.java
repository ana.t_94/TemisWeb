/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author inftel02
 */
@Entity
@Table(name = "APARTMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Apartment.findAll", query = "SELECT a FROM Apartment a")
    , @NamedQuery(name = "Apartment.findByAddress", query = "SELECT a FROM Apartment a WHERE a.address = :address")
    , @NamedQuery(name = "Apartment.findByName", query = "SELECT a FROM Apartment a WHERE a.name = :name")
    , @NamedQuery(name = "Apartment.findByPot", query = "SELECT a FROM Apartment a WHERE a.pot = :pot")
    , @NamedQuery(name = "Apartment.findByDescription", query = "SELECT a FROM Apartment a WHERE a.description = :description")
    , @NamedQuery(name = "Apartment.findById", query = "SELECT a FROM Apartment a WHERE a.id = :id")})
@SequenceGenerator(name="apartmentsequence", initialValue=1, allocationSize=10)
public class Apartment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "ADDRESS")
    private String address;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "POT")
    private BigDecimal pot;
    @Size(max = 200)
    @Column(name = "DESCRIPTION")
    private String description;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="apartmentsequence")
    private BigDecimal id;
    @ManyToMany(mappedBy = "apartmentCollection", cascade = CascadeType.PERSIST)
    private Collection<User> userCollection;
    @OneToMany(mappedBy = "idApartment")
    private Collection<Debt> debtCollection;

    public Apartment() {
    }

    public Apartment(BigDecimal id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPot() {
        return pot;
    }

    public void setPot(BigDecimal pot) {
        this.pot = pot;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    @XmlTransient
    public Collection<Debt> getDebtCollection() {
        return debtCollection;
    }

    public void setDebtCollection(Collection<Debt> debtCollection) {
        this.debtCollection = debtCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Apartment)) {
            return false;
        }
        Apartment other = (Apartment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelEjb.Apartment[ id=" + id + " ]";
    }
    
}
