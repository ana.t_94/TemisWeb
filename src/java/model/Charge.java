/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author inftel02
 */
@Entity
@Table(name = "CHARGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Charge.findAll", query = "SELECT c FROM Charge c")
    , @NamedQuery(name = "Charge.findByPaid", query = "SELECT c FROM Charge c WHERE c.paid = :paid")
    , @NamedQuery(name = "Charge.findByAmount", query = "SELECT c FROM Charge c WHERE c.amount = :amount")
    , @NamedQuery(name = "Charge.findByName", query = "SELECT c FROM Charge c WHERE c.name = :name")
    , @NamedQuery(name = "Charge.findById", query = "SELECT c FROM Charge c WHERE c.id = :id")})
@SequenceGenerator(name="chargesequence", initialValue=1, allocationSize=10)
public class Charge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "PAID")
    private Character paid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="chargesequence")
    private BigDecimal id;
    @JoinColumn(name = "ID_DEBT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Debt idDebt;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID")
    @ManyToOne
    private User idUser;

    public Charge() {
    }

    public Charge(BigDecimal id) {
        this.id = id;
    }

    public Character getPaid() {
        return paid;
    }

    public void setPaid(Character paid) {
        this.paid = paid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Debt getIdDebt() {
        return idDebt;
    }

    public void setIdDebt(Debt idDebt) {
        this.idDebt = idDebt;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Charge)) {
            return false;
        }
        Charge other = (Charge) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelEjb.Charge[ id=" + id + " ]";
    }
    
}
