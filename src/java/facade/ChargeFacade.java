/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Charge;

/**
 *
 * @author inftel02
 */
@Stateless
public class ChargeFacade extends AbstractFacade<Charge> {

    @PersistenceContext(unitName = "TemisWebPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ChargeFacade() {
        super(Charge.class);
    }
    
}
