/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Utils;
import model.Apartment;
import model.User;
import org.jboss.weld.util.collections.ArraySet;

@Stateless
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext(unitName = "TemisWebPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    public User getUser(String username, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {                   
        try {
            Query query = em.createQuery("SELECT u FROM User u WHERE u.username = :username");

            query.setParameter("username", username);
            User user = (User) query.getSingleResult();
            
            return Utils.validatePassword(password, user.getPassword()) ? user : null;
        } catch (NoResultException e) {
            return null;
        }
        
    }
    
    public Collection<User> getUserNotInApartment( Apartment apartment){
        Collection<BigDecimal> collection = new ArraySet<>();
        apartment.getUserCollection().forEach((u)->{
            collection.add(u.getId());
        });
        Query query = em.createQuery("SELECT us From User us where us.id NOT IN :users");
        query.setParameter("users", collection);
        return (Collection<User>)query.getResultList();
    }

}
