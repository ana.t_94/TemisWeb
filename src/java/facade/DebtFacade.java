/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Debt;

/**
 *
 * @author inftel02
 */
@Stateless
public class DebtFacade extends AbstractFacade<Debt> {

    @PersistenceContext(unitName = "TemisWebPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DebtFacade() {
        super(Debt.class);
    }
    
}
