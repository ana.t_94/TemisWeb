/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.UserFacade;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Apartment;
import model.User;
import model.Utils;
import distpatcher.distpatcher;

public class UserController {

    private final UserFacade userFacade;
    private HttpServletRequest request;
    private HttpServletResponse response;
    
    private static final String messageLoginError = "Invalid user or password";
    private static final String messageErrorEmpty = "Something obligatory field is empty";
    private static final String messageRegisterErrorMatch = "Password not match";
    private static final String messageRegisterErrorTaken = "This username is already taken";

    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    public void loginControl() throws ServletException, IOException {
        try {
            String username = request.getParameter("user");
            String password = request.getParameter("password");
            HttpSession session = request.getSession();
            User userLogin = userFacade.getUser(username, password);
            session.setAttribute("userLogin", userLogin);
            if (userLogin != null) {
                Collection<Apartment> apartments = userLogin.getApartmentCollection();
                request.setAttribute("apartments", apartments);
                request.getRequestDispatcher("/groups.jsp").include(request, response);
            } else {
                request.setAttribute("messageError", messageLoginError);
                request.getRequestDispatcher("/index.jsp").include(request, response);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(distpatcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(distpatcher.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void registerControl() throws ServletException, IOException {
        String name = request.getParameter("name").trim();
        String surnames = request.getParameter("surname").trim();
        String username = request.getParameter("username").trim();
        String password = request.getParameter("password").trim();
        String passwordConfirm = request.getParameter("passwordConfirm");

        try {
            if (password.trim().equals("") || name.trim().equals("") || surnames.trim().equals("") || username.trim().equals("")) {
                request.setAttribute("messageError", messageErrorEmpty);
                request.getRequestDispatcher("/newUser.jsp").include(request, response);
            } else if (password.equals(passwordConfirm)) {
                User user = new User(null, Utils.generateStorngPasswordHash(password), username);
                user.setName(name);
                user.setSurname(surnames);
                userFacade.create(user);
                request.getRequestDispatcher("/index.jsp").include(request, response);
            } else {
                request.setAttribute("messageError", messageRegisterErrorMatch);
                request.setAttribute("name", name);
                request.setAttribute("surname", surnames);
                request.setAttribute("username", username);
                request.getRequestDispatcher("/newUser.jsp").include(request, response);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | EJBException ex) {
            request.setAttribute("messageError", messageRegisterErrorTaken);
            request.setAttribute("name", name);
            request.setAttribute("surname", surnames);
            request.getRequestDispatcher("/newUser.jsp").include(request, response);
        }
    }

    public void modifyUserControl() throws IOException, ServletException {
        String name = request.getParameter("name").trim();
        String surnames = request.getParameter("surname").trim();
        String password = request.getParameter("password").trim();
        String passwordConfirm = request.getParameter("confirmPassword");

        User user = (User) request.getSession().getAttribute("userLogin");

        if (password.equals(passwordConfirm)) {
            try {
                user.setPassword(Utils.generateStorngPasswordHash(password));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(distpatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(distpatcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.removeAttribute("messageError");
        } else {
            request.setAttribute("messageError", messageRegisterErrorMatch);
        }
        user.setName(name);
        user.setSurname(surnames);
        userFacade.edit(user);
        request.getRequestDispatcher("/user.jsp").forward(request, response);
    }

    public void goManageUsersControl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("users", ((Apartment) request.getSession().getAttribute("selectedApartment")).getUserCollection());
        request.setAttribute("usersNotInGroup", userFacade.getUserNotInApartment(((Apartment) request.getSession().getAttribute("selectedApartment"))));
        request.getRequestDispatcher("/adminUser.jsp").include(request, response);
    }

    public void manageUsersDeleteControl() throws ServletException, IOException {
        User user = userFacade.find(BigDecimal.valueOf(Long.parseLong(request.getParameterValues("adminUserUser")[0])));
        Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");
        Collection<User> collection = apartment.getUserCollection();
        collection.removeIf((u) -> {
            return u.getId().equals(user.getId());
        });
        apartment.setUserCollection(collection);
        userFacade.edit(user);
        goManageUsersControl(request, response);
    }

    public void manageUsersAddControl() throws ServletException, IOException {
        User user = userFacade.find(BigDecimal.valueOf(Long.parseLong(request.getParameterValues("adminUserUser")[0])));
        Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");
        apartment.getUserCollection().add(user);
        user.getApartmentCollection().add(apartment);
        userFacade.edit(user);
        goManageUsersControl(request, response);
    }

    public void goManageUsersControl() throws ServletException, IOException {
        request.setAttribute("users", ((Apartment) request.getSession().getAttribute("selectedApartment")).getUserCollection());
        request.setAttribute("usersNotInGroup", userFacade.getUserNotInApartment(((Apartment) request.getSession().getAttribute("selectedApartment"))));
        request.getRequestDispatcher("/adminUser.jsp").include(request, response);
    }

    public void manageUsersBack() throws ServletException, IOException {
        Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");
        request.setAttribute("debts", apartment.getDebtCollection());
        request.getRequestDispatcher("/apartment.jsp").include(request, response);
    }
    
    public void setDistPatch(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }

}
