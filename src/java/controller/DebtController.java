/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ApartmentFacade;
import facade.ChargeFacade;
import facade.DebtFacade;
import facade.UserFacade;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Apartment;
import model.Charge;
import model.Debt;
import model.Utils;
import distpatcher.distpatcher;

public class DebtController {

    private final DebtFacade debtFacade;
    private final ChargeFacade chargeFacade;
    private final UserFacade userFacade;
    private final ApartmentFacade apartmentFacade;

    private HttpServletRequest request;
    private HttpServletResponse response;

    public DebtController(DebtFacade debtFacade, ChargeFacade chargeFacade, ApartmentFacade apartmentFacade, UserFacade userFacade) {
        this.debtFacade = debtFacade;
        this.chargeFacade = chargeFacade;
        this.userFacade = userFacade;
        this.apartmentFacade = apartmentFacade;
    }

    public void selectDebtControl() throws ServletException, IOException {
        HttpSession session = request.getSession();
        Apartment apartment = (Apartment) session.getAttribute("selectedApartment");

        Debt debt = null;
        for (Debt d : apartment.getDebtCollection()) {
            if (d.getId().equals(BigDecimal.valueOf(Long.parseLong(request.getParameter("id"))))) {
                debt = d;
            }
        }

        session.setAttribute("selectedDebt", debt);
        request.setAttribute("date", Utils.dateToString(debt.getDate()));
        request.getRequestDispatcher("/debt.jsp").include(request, response);
    }

    public void goDebtControl() throws ServletException, IOException {
        request.setAttribute("users", ((Apartment) request.getSession().getAttribute("selectedApartment")).getUserCollection());
        request.getRequestDispatcher("/newDebt.jsp").include(request, response);
    }

    public void createDebtControl() throws ServletException, IOException {
        try {
            String name = request.getParameter("name");
            Double totalCount = Double.parseDouble(request.getParameter("totalCount"));
            String description = request.getParameter("description");
            String dateString = request.getParameter("date");
            Date date = Utils.stringToDate(dateString);

            Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");
            Debt debt = new Debt();
            debt.setCounttotal(BigDecimal.valueOf(totalCount));
            debt.setDate(date);
            debt.setDescription(description);
            debt.setName(name);
            debtFacade.create(debt);
            debt.setIdApartment(apartment);
            apartment.getDebtCollection().add(debt);
            debtFacade.edit(debt);

            request.setAttribute("debts", apartment.getDebtCollection());
            request.getRequestDispatcher("/apartment.jsp").include(request, response);

        } catch (ParseException ex) {
            Logger.getLogger(distpatcher.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void debtDeleteControl() throws ServletException, IOException {
        Debt debt = ((Debt) request.getSession().getAttribute("selectedDebt"));
        Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");

        for (Charge charge : debt.getChargeCollection()) {
            chargeFacade.remove(charge);
        }
        debtFacade.remove(debt);
        apartment.getDebtCollection().remove(debt);
        apartmentFacade.edit(apartment);

        request.setAttribute("debts", apartment.getDebtCollection());
        request.getRequestDispatcher("/apartment.jsp").include(request, response);
    }

    public void setDistPatch(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }
}
