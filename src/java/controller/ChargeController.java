/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ChargeFacade;
import facade.DebtFacade;
import facade.UserFacade;
import java.io.IOException;
import java.math.BigDecimal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Apartment;
import model.Charge;
import model.Debt;
import model.User;
import model.Utils;

public class ChargeController {

    private final ChargeFacade chargeFacade;
    private final DebtFacade debtFacade;
    private final UserFacade userFacade;
    
    private HttpServletRequest request;
    private HttpServletResponse response;
    
    public ChargeController(ChargeFacade chargeFacade, DebtFacade debtFacade, UserFacade userFacade) {
        this.chargeFacade = chargeFacade;
        this.debtFacade = debtFacade;
        this.userFacade = userFacade;
    }

    public void selectChargeControl() throws ServletException, IOException {
        Debt debt = (Debt) request.getSession().getAttribute("selectedDebt");

        Charge charge = null;
        for (Charge c : debt.getChargeCollection()) {
            if (c.getId().equals(BigDecimal.valueOf(Long.parseLong(request.getParameter("id"))))) {
                charge = c;
            }
        }

        request.getSession().setAttribute("selectedCharge", charge);
        request.getRequestDispatcher("/charge.jsp").include(request, response);
    }

    public void chargeModifyControl() throws ServletException, IOException {
        String paid = request.getParameter("paid");
        Charge charge = ((Charge) request.getSession().getAttribute("selectedCharge"));
        charge.setPaid(paid != null ? '1' : '0');
        chargeFacade.edit(charge);
        chargeBack();
    }

    public void chargeBack() throws ServletException, IOException {
        Debt debt = (Debt) request.getSession().getAttribute("selectedDebt");
        request.setAttribute("date", Utils.dateToString(debt.getDate()));
        request.getRequestDispatcher("/debt.jsp").include(request, response);
    }

    public void chargeDeleteControl() throws ServletException, IOException {
        Charge charge = ((Charge) request.getSession().getAttribute("selectedCharge"));
        Debt debt = ((Debt) request.getSession().getAttribute("selectedDebt"));
        debt.getChargeCollection().remove(charge);
        chargeFacade.remove(charge);
        debtFacade.edit(debt);
        chargeBack();
    }

    public void goChargeControl() throws ServletException, IOException {
        request.setAttribute("users", ((Apartment) request.getSession().getAttribute("selectedApartment")).getUserCollection());
        request.getRequestDispatcher("/newCharge.jsp").include(request, response);
    }

    public void createChargeControl() throws ServletException, IOException {
        String name = request.getParameter("name");
        Double amount = Double.parseDouble(request.getParameter("amount"));
        User user = userFacade.find(BigDecimal.valueOf(Long.parseLong(request.getParameterValues("user")[0])));
        Debt debt = (Debt) request.getSession().getAttribute("selectedDebt");

        Charge charge = new Charge();
        charge.setAmount(BigDecimal.valueOf(amount));
        charge.setName(name);
        charge.setPaid('0');
        charge.setIdUser(user);
        charge.setIdDebt(debt);
        debt.getChargeCollection().add(charge);

        chargeFacade.create(charge);
        chargeBack();
    }
    
    public void setDistPatch(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }
}
