/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.ApartmentFacade;
import facade.ChargeFacade;
import facade.DebtFacade;
import facade.UserFacade;
import java.io.IOException;
import java.math.BigDecimal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Apartment;
import model.Charge;
import model.Debt;
import model.User;

public class ApartmentController {

    private final ApartmentFacade apartmentFacade;
    private final UserFacade userFacade;
    private final ChargeFacade chargeFacade;
    private final DebtFacade debtFacade;
    
    private HttpServletRequest request;
    private HttpServletResponse response;
    
    public ApartmentController(ApartmentFacade apartmentFacade, UserFacade userFacade, ChargeFacade chargeFacade, DebtFacade debtFacade) {
        this.apartmentFacade = apartmentFacade;
        this.userFacade = userFacade;
        this.chargeFacade = chargeFacade;
        this.debtFacade = debtFacade;
    }

    public void createApartmentControl() throws ServletException, IOException {
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        Double pot = Double.parseDouble(request.getParameter("pot"));
        String description = request.getParameter("description");
        User user = (User) request.getSession().getAttribute("userLogin");

        Apartment apartment = new Apartment();
        apartment.setAddress(address);
        apartment.setDescription(description);
        apartment.setName(name);
        apartment.setPot(BigDecimal.valueOf(pot));
        apartmentFacade.create(apartment);

        user.getApartmentCollection().add(apartment);
        apartment.getUserCollection().add(user);

        userFacade.edit(user);
        myApartmentsBC();
    }

    public void selectApartmentControl() throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("userLogin");

        Apartment apartment = null;
        for (Apartment a : user.getApartmentCollection()) {
            if (a.getId().equals(BigDecimal.valueOf(Long.parseLong(request.getParameter("id"))))) {
                apartment = a;
            }
        }

        session.setAttribute("selectedApartment", apartment);
        request.getRequestDispatcher("/apartment.jsp").include(request, response);
    }

    public void apartmentDeleteControl() throws ServletException, IOException {
        Apartment apartment = (Apartment) request.getSession().getAttribute("selectedApartment");
        User user = ((User) request.getSession().getAttribute("userLogin"));
        for (Debt d : apartment.getDebtCollection()) {
            for (Charge c : d.getChargeCollection()) {
                chargeFacade.remove(c);
            }
            debtFacade.remove(d);
        }

        apartmentFacade.remove(apartment);
        user.getApartmentCollection().remove(apartment);
        userFacade.edit(user);
        myApartmentsBC();
    }

    public void myApartmentsBC() throws ServletException, IOException {
        request.getRequestDispatcher("/groups.jsp").include(request, response);
    }
    
    public void setDistPatch(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }

}
