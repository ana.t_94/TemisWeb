<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; Charge" />
        </jsp:include>
        
        <script src="js/jquery.min.js" type="text/javascript"></script>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul2 class="breadcrumb">
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=manageUsersBack'">${selectedApartment.name}</a></li>
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=chargeBack'">${selectedDebt.name}</a></li>
                                    <li>${selectedCharge.name}</li>
                                </ul2>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>${selectedCharge.name}</h3>
                            <form id="chargeForm" action="distpatcher" method="POST">
                                <input type="hidden" id="actionForm" name="action" >
                                <br><label>Amount</label><label2 name="chargeAmount">${selectedCharge.amount}&euro;</label2>
                                <br><label>User</label><label2 name="chargeUser">${selectedCharge.idUser.username}</label2>
                                    <c:choose>
                                        <c:when test="${selectedCharge.paid eq '1'}"><br><label><input type="checkbox" name="paid" style="color:#e89314;" checked>  Paid</label></c:when>
                                    <c:otherwise><br><label><input type="checkbox" name="paid" style="color:#e89314;">  Paid</label></c:otherwise>
                                    </c:choose>    

                                <div align="center">
                                    <button id="backButton" class="button-style" type="button">Cancel</button><button id="modifyButton" class="button-style" type="button">Accept</button><button id="deleteButton" class="button-style" type="button">Delete</button>
                                </div>
                            </form>
                        </div> 
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
    <script>
        jQuery(document).ready(function ($) {
            $("#backButton").click(function () {
                $("#actionForm").val("chargeBack");
                $("#chargeForm").submit();
            });
            $("#modifyButton").click(function () {
                $("#actionForm").val("chargeModify");
                $("#chargeForm").submit();
            });
            $("#deleteButton").click(function () {
                $("#actionForm").val("chargeDelete");
                $("#chargeForm").submit();
            });
        });
    </script>
</html>
