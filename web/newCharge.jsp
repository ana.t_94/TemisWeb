
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; New Charge" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul2 class="breadcrumb">
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=manageUsersBack'">${selectedApartment.name}</a></li>
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=chargeBack'">${selectedDebt.name}</a></li>
                                    <li>New Charge</li>
                                </ul2>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->


            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>New Charge</h3>
                        </div>

                        <form action="distpatcher" method="POST">
                            <div align="center">
                                <input name="action" type="hidden" value="createCharge"/>
                                <br><br><label>Name</label><input type="text" style="color:#e89314" name="name" required>
                                <br><label>Amount</label><input type="number" step="any" style="color:#e89314" name="amount" required>
                                <br><label>User</label><select name="user" style="color:#e89314" size="1" required>
                                    <c:forEach var="user" items="${users}">
                                        <option value="${user.id}">${user.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div align="center">
                                <button class="button-style" type="button" onClick="parent.location = 'distpatcher?action=chargeBack'">Cancel</button><button class="button-style">Create</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>