<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; About Us" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">

            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;"></a>
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li class="active">
                                        <a>About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>We have just started!</h3>
                            <p>We only have at your disposal the <b>desktop application</b> version (much more <b>complete</b> than this one, to tell the truth).
                                Soon we will be adding new improvements, so <b>stay tuned! <font color="#e89314">:)</font></b></p>
                            <p>Meanwhile, you may be interested in where our <b>name</b> comes from...</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 animate-box fadeInUp animated">
                            <div class="fh5co-cover" style="background-image:url(images/themis.jpg); background-size: 1000px 700px; background-repeat: no-repeat;">
                                <div class="desc">
                                    <p><i>Temis</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 animate-box fadeInUp animated">
                            <div class="fh5co-cover">
                                <div class="desc">
                                    <p><b>Temis</b> is described as <i>the Lady of good counsel</i>, and is the personification of <b>divine order</b>, fairness, <b>law</b>, natural law, and custom. Her symbols are the Scales of Justice, tools used to remain <b>balanced</b> and pragmatic. Themis means "divine law" rather than human ordinance, literally <b><i>that which is put in place</i></b>. This is why we thought it was a good idea to use the name of this goddess.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>