<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; Admin Users" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul2 class="breadcrumb">
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=manageUsersBack'">${selectedApartment.name}</a></li>
                                    <li>Manage Users</li>
                                </ul2>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>Who is going?</h3>
                            <form action="distpatcher" method="POST">
                                <input type="hidden" name="action" value="manageUsersDelete"/>
                                <select name="adminUserUser" style="color:#e89314" size="1">
                                    <c:forEach var="user" items="${users}">
                                        <option value="${user.id}">${user.name}</option>
                                    </c:forEach>
                                </select>
                                <button class="button-style">Bye!</button>
                            </form>
                        </div>
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>...or who is new?</h3>
                            <form action="distpatcher" method="POST">
                                <input type="hidden" name="action" value="manageUsersAdd"/>
                                <select name="adminUserUser" style="color:#e89314" size="1">
                                    <c:forEach var="user" items="${usersNotInGroup}">
                                        <option value="${user.id}">${user.name}</option>
                                    </c:forEach>
                                </select>
                                <button class="button-style">Hello!</button>
                            </form>
                        </div>
                    </div>
                    <div class="new">
                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=manageUsersBack'">Maybe you didn't want to do anything...</a>
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>
