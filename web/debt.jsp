<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; Apartment" />
        </jsp:include>
        
        <script src="js/jquery.min.js" type="text/javascript"></script>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul2 class="breadcrumb">
                                    <li><a class="a-pointer" onClick="parent.location = 'distpatcher?action=manageUsersBack'">${selectedApartment.name}</a></li>
                                    <li>${selectedDebt.name}</li>
                                </ul2>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>${selectedDebt.name}</h3>
                            <form>
                                <label>Total Count</label><label2 name="debtTotalCount">${selectedDebt.counttotal}&euro;</label2>
                                <label>Date</label><label2 name="debtDate">${date}</label2>
                                <br><label>Description</label><textarea text-align="center" readonly="readonly" disabled="disabled" name="apartmentDescription" rows="3" cols="23" style="color:#e89314; vertical-align:middle; resize:none;">${selectedDebt.description}</textarea>
                            </form>
                            <a class="a-pointer" onClick="parent.location = 'distpatcher?action=debtDelete'">(Delete this debt)</a>
                        </div>
                        <div class="col-md-9 animate-box fadeInUp animated">
                            <table width="100%" class="clickable-table">
                                <thead>
                                    <tr class="table_title">
                                        <th>Name</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>Paid</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="charge" items="${selectedDebt.chargeCollection}">
                                        <tr class="clickable-row" data-href="distpatcher?id=${charge.id}&action=selectCharge">                   
                                            <td>${charge.name}</td>
                                            <td>${charge.idUser.username}</td>
                                            <td>${charge.amount}&euro;</td>
                                            <c:choose>
                                                <c:when test="${charge.paid eq '1'}"><td>Paid</td></c:when>
                                                <c:otherwise><td>Not Paid</td></c:otherwise>
                                            </c:choose>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3 animate-box fadeInUp animated">
                            <button type="button" onClick="parent.location = 'distpatcher?action=goNewCharge'" class="button-style" type="button">New Charge</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
</html>