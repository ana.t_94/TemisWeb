<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${param.pagetitle}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="css/themify-icons.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/style.css">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>

<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>

<link rel="icon" type="images/png" href="images/favicon.png">