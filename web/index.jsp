<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; Login" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <div id="fh5co-header">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <img src="images/logo.png" style="width:850px;height:400px;">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated col-md-8 col-md-offset-2 text-center">
                        <c:if test="${messageError != null}">
                            <br><br><div align="center" class="col-md-offset alert alert-danger">
                                <strong>${messageError}</strong>
                            </div>
                        </c:if>
                        <form action="distpatcher" method="POST">
                            <div class="col-md-offset">
                                <input type="hidden" name="action" value="login">
                                <input id="inputUser" name="user" type="text" class="form-control" style="color:#e89314" placeholder="User" required>
                                <input id="inputPassword" name="password" type="password" class="form-control" style="color:#e89314" placeholder="Password" required>
                            </div>

                            <div class="col-md-offset">
                                <button type="button" class="button-style" onClick="parent.location = 'newUser.jsp'" >Register</button><button class="button-style">Login</button>
                            </div>
                        </form>

                        <div class="col-md-offset text-center heading-section">
                            <h3>Finances updated =  Better relationships!</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of media-section -->

        </div>
        <!-- End of media section -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>
