<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; New Apartment" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>New Apartment</h3>
                        </div>

                        <form action="distpatcher" method="POST" accept-charset="ISO-8859-1">
                            <div align="center">
                                <input name="action" type="hidden" value="createApartment"/>
                                <br><br><label>Name</label><input type="text" style="color:#e89314" name="name" required>
                                <br><label>Address</label><input type="text" style="color:#e89314" name="address" required>
                                <br><label>Pot</label><input type="number" step="any" style="color:#e89314" name="pot" required>
                                <br><label>Description</label><textarea name="description" rows="3" cols="23" style="color:#e89314; vertical-align:middle; resize:none;" required></textarea>
                            </div>
                            <div align="center">
                                <button class="button-style" type="button" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">Cancel</button><button class="button-style">Create!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>