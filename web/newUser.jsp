<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; New User" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <img src="images/logo.png" style="width:650px;height:250px;">
                        </div>
                    </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>New User</h3>
                        </div>

                        <form action="distpatcher" method="POST" accept-charset="ISO-8859-1">
                            <c:if test="${messageError != null}">
                                <br><br><div align="center" class="col-md-8 col-md-offset-2 alert alert-danger">
                                    <strong>${messageError}</strong>
                                </div>
                            </c:if>
                            <input type="hidden" name="action" value="register">
                            <div class="col-md-8 col-md-offset-2" align="center">
                                <label>Name</label><input type="text" style="color:#e89314" name="name" value="<c:if test="${name != null}"> ${name} </c:if>" required>
                                <br><label>Surname</label><input type="text" style="color:#e89314" name="surname" value="<c:if test="${surname != null}"> ${surname} </c:if>" required>
                                <br><label>Username</label><input type="text" style="color:#e89314" name="username" value="<c:if test="${username != null}"> ${username} </c:if>" required>
                                <br><label>Password</label><input type="password" style="color:#e89314" name="password" required>
                                <br><label>Confirm Password</label><input type="password" style="color:#e89314" name="passwordConfirm" required>
                            </div>

                            <div class="col-md-8 col-md-offset-2" align="center">
                                <button type="button" class="button-style" onClick="parent.location = 'index.jsp'">Cancel</button><button class="button-style">Register</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- End of media-section -->

        </div>
        <!-- End of media section -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>
