<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; My Profile" />
        </jsp:include>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li>
                                        <a class="a-pointer" onClick="parent.location = 'distpatcher?action=myApartmentsBC'">My apartments</a>
                                    </li>
                                    <li class="active">
                                        <a>My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">

                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>This is your personal information...</h3>
                            <p>Something do you want to change?</p>
                        </div>
                        <form action="distpatcher" method="POST" accept-charset="ISO-8859-1">
                            <c:if test="${messageError != null}">
                                <br><br><div align="center" class="col-md-8 col-md-offset-2 alert alert-danger">
                                    <strong>${messageError}</strong>
                                </div>
                            </c:if>
                            <div align="center" class="col-md-8 col-md-offset-2">
                                <input type="hidden" name="action" value="modifyUser">
                                <br><label>Name</label><input type="text" style="color:#e89314" name="name" value="${userLogin.name}" required>
                                <br><label>Surname</label><input type="text" style="color:#e89314" name="surname" value="${userLogin.surname}" required>
                                <br><label>Username</label><label2>${userLogin.username}</label2>
                                <br><label>Password</label><input type="password" style="color:#e89314" name="password" required>
                                <br><label>Confirm Password</label><input type="password" style="color:#e89314" name="confirmPassword" required>
                            </div>

                            <div align="center" class="col-md-8 col-md-offset-2">
                                <button class="button-style">Accept</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
</html>
