<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="inc/head.jsp" >
            <jsp:param name="pagetitle" value="Temis &mdash; Groups" />
        </jsp:include>
        
        <script src="js/jquery.min.js" type="text/javascript"></script>
    </head>
    <!-- End of head -->

    <body>
        <div class="box-wrap">
            <header role="banner" id="fh5co-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fh5co-navbar-brand">
                                    <img src="images/logo.png" style="width:300px;height:130px;">
                                </div>
                            </div>
                            <div class="col-md-9 main-nav">
                                <ul class="nav text-right">
                                    <li class="active">
                                        <a>My apartments</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="user.jsp">My profile</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="about.jsp">About Us</a>
                                    </li>
                                    <li>
                                        <a class="a-pointer" href="index.jsp">Log out</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <!-- End of header -->

            <div id="fh5co-media-section">
                <div class="container">
                    <div class="row animate-box fadeInUp animated">
                        <div class="col-md-8 col-md-offset-2 text-center heading-section">
                            <h3>Welcome ${userLogin.name}!</h3>
                            <p>These are your apartments...</p>
                            <table width="100%" class="clickable-table">
                                <thead>
                                    <tr class="table_title">
                                        <th>Name</th>
                                        <th>Address</th> 
                                        <th>Pot</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="apartment" items="${userLogin.apartmentCollection}">
                                        <tr class="clickable-row" data-href="distpatcher?id=${apartment.id}&action=selectApartment">
                                            <td>${apartment.name}</td>
                                            <td>${apartment.address}</td>
                                            <td>${apartment.pot}&euro;</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="new">
                        <a class="a-pointer" href="newApartment.jsp">Create a new apartment!</a>
                    </div>
                </div>
            </div>
            <!-- End of media section -->

        </div>
        <!-- End of box -->

        <%@ include file="WEB-INF/jspf/footer.jspf" %>
        <!-- End of footer -->

    </body>
    <!-- End of body -->
    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
</html>
